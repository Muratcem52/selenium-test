
import java.util.regex.Pattern;
import java.util.concurrent.TimeUnit;
import org.junit.*;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;

public class Amazon {
  private WebDriver driver;
  private String baseUrl;
  private boolean acceptNextAlert = true;
  private StringBuffer verificationErrors = new StringBuffer();

  @Before
  public void setUp() throws Exception {
    driver = new FirefoxDriver();
    baseUrl = "https://www.amazon.com/";
    driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
  }

  @Test
  public void testAmazon() throws Exception {
    driver.get(baseUrl + "/");
    driver.findElement(By.cssSelector("#nav-link-accountList > span.nav-line-2")).click();
    driver.findElement(By.id("ap_email")).clear();
    driver.findElement(By.id("ap_email")).sendKeys("mcemyazici@hotmail.com");
    driver.findElement(By.id("ap_password")).clear();
    driver.findElement(By.id("ap_password")).sendKeys("muratcem52");
    driver.findElement(By.id("signInSubmit")).click();
    driver.findElement(By.id("twotabsearchtextbox")).clear();
    driver.findElement(By.id("twotabsearchtextbox")).sendKeys("samsung");
    driver.findElement(By.cssSelector("input.nav-input")).click();
    try {
      assertThat("of 0 results for \"samsung\"", is(not(driver.findElement(By.id("s-result-count")).getText())));
    } catch (Error e) {
      verificationErrors.append(e.toString());
    }
    driver.findElement(By.linkText("2")).click();
    try {
      assertEquals("2", driver.findElement(By.cssSelector("span.pagnCur")).getText());
    } catch (Error e) {
      verificationErrors.append(e.toString());
    }
    driver.findElement(By.xpath("//li[@id='result_26']/div/div/div/div[2]/div/div/a/h2")).click();
    driver.findElement(By.id("add-to-wishlist-button")).click();
    // ERROR: Caught exception [ERROR: Unsupported command [clickAt | id=atwl-list-name-1KCG6S8DK95L0 | ]]
    String product = driver.findElement(By.id("productTitle")).getText();
    String productname = driver.findElement(By.cssSelector("li.w-asin > table > tbody > tr > td > a")).getText();
    // ERROR: Caught exception [ERROR: Unsupported command [getEval | storedVars['product']==storedVars['productname'] | ]]
    driver.findElement(By.cssSelector("span.w-button-text")).click();
    driver.findElement(By.name("submit.deleteItem")).click();
    // ERROR: Caught exception [ERROR: Unsupported command [getAllWindowTitles |  | ]]
  }

  @After
  public void tearDown() throws Exception {
    driver.quit();
    String verificationErrorString = verificationErrors.toString();
    if (!"".equals(verificationErrorString)) {
      fail(verificationErrorString);
    }
  }

  private boolean isElementPresent(By by) {
    try {
      driver.findElement(by);
      return true;
    } catch (NoSuchElementException e) {
      return false;
    }
  }

  private boolean isAlertPresent() {
    try {
      driver.switchTo().alert();
      return true;
    } catch (NoAlertPresentException e) {
      return false;
    }
  }

  private String closeAlertAndGetItsText() {
    try {
      Alert alert = driver.switchTo().alert();
      String alertText = alert.getText();
      if (acceptNextAlert) {
        alert.accept();
      } else {
        alert.dismiss();
      }
      return alertText;
    } finally {
      acceptNextAlert = true;
    }
  }
}
